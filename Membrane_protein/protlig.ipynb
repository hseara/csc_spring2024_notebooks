{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial on protein–small molecule and lipid–protein interactions\n",
    "\n",
    "In this tutorial, we will set up a simulation system containing a membrane protein, the bound small molecule, the host lipid membrane, and the solvent environment. This will be performed using the [CHARMM-GUI](https://www.charmm-gui.org/) portal. The process of setting up simulations for lipid systems, soluble proteins, or nanomaterils is very similar. Then, we will skip the simulation stage (~2 weeks on Puhti per system), download pre-computed trajectories for this system, and analyze them. Towards the end of the tutorial, we will also analyze an additional systems, listed below. \n",
    "\n",
    "## The workflow in a nutshell\n",
    "\n",
    "* Generate a simulation setup with the help of CHARMM-GUI\n",
    "     * Upload the protein, pre-process the protein PDB\n",
    "     * Set up lipid and solvent environments\n",
    "     * Set up the ligand parametrization\n",
    "     * Download files, equilibrate locally\n",
    "* Download pre-computed trajectories from Zenodo\n",
    "* Analyze the simulations as far as you get\n",
    "    * Protein behavior: RMSD, RMSF, distances\n",
    "    * Protein--small molecule interactions: hydrogen bonding, distances\n",
    "    * Effect of amino acid mutation on ligand binding\n",
    "\n",
    "## Learning outcomes\n",
    "\n",
    "* Set up simulations for (membrane) proteins and ligands in CHARMM-GUI\n",
    "* Perform basic analyses using the built-in tools of GROMACS\n",
    "* Perform more advanced analyses using the MDAnalysis library\n",
    "\n",
    "## The science behind\n",
    "\n",
    "This tutorial is based on [the recent publication](https://doi.org/10.1101/2022.07.03.498529) describing the mechanism of inhibition of the Sec61 translocon by a cotransin derivative. During the biogenesis of membrane proteins and secreted proteins, the translating ribosome sits attached to the transmembrane Sec61 protein complex and pushes the nascent polypeptide chain (unfolded protein) through the Sec61 tunnel either into the ER lumen (secreted and thus soluble proteins exit Sec61 from the end of the tunnel) or to the ER membrane (membrane proteins escape via the lateral gate between the two halves of a clam-like Sec61 structure facing the membrane). As many proteins processed by Sec61 are of therapeutic interest (viral proteins, inflammatory cytokines, growth factor receptors related to cancer to name a few), the substrate-selective inhibition of Sec61 is desireable.\n",
    "\n",
    "Cotransin is a cyclic depsipeptide that has been demonstrated to inhibit the translocation of proteins from the ribosome into the ER during co-translational translocation. The cotransin derivative, KZR8445 used in this work, was found to be substrate-selective using biochemical assays. Moreover, it was efficacious towards rheumatoid arthritis in mice, and inhibited the replication of SARS-CoV-2 and the production of its spike protein. Thus, to understand its mechanism of function could help further refine the selectivity of cotransins towards new and specific protein targets.\n",
    "\n",
    "To explain the substrate-selective inhibition, cryo-EM of KZR8445-bound Sec61 was performed, and additional density was found at the Sec61 lateral gate, i.e. at the site where the transmembrane proteins exit the channel. KZR8445 was modelled into that density. Here, we will use this structure of the Sec61 bound by KZR8445. We will study the effects of the inhibitor on the protein structure, notably its dynamics and the conformation of the lateral gate. Then, we will find the key hydrogen bonding partners for KZR8445, and confirm their importance using computational mutagenesis. We will also study some membrane properties, namely lipid flip-flops and the membrane structure.\n",
    "\n",
    "<img src=\"figs/cryoem.png\" alt=\"fishy\" class=\"bg-primary\" width=\"500px\">\n",
    "(Rehan et al., bioRxiv (2022), DOI: https://doi.org/10.1101/2022.07.03.498529)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import MDAnalysis as mda\n",
    "from MDAnalysis.analysis import distances\n",
    "import matplotlib.pyplot as plt\n",
    "import nglview as nv\n",
    "from ipywidgets import widgets\n",
    "from IPython.display import display\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's visualize the protein & the inhibitor first!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = mda.Universe('Sec61_KZR8445.pdb')\n",
    "view = nv.show_mdanalysis(u)\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Now let's learn how to set the system in CHARMM-GUI:\n",
    "\n",
    "* Go to [CHARMM-GUI](https://www.charmm-gui.org/) and log in\n",
    "* Note: along the process, you can always look at the generated structure in the browser!\n",
    "* Go to input generator -> Membrane builder -> Bilayer builder\n",
    "* Typically, we could use a PDB identifier here. We will use a structure coresponding to [PDB entry 7ZL3](https://www.rcsb.org/structure/7ZL3), but to have matching atom names for the ligand parametrization, we will upload the local PDB \"Sec61_KZR8445.pdb\" and **continue**\n",
    "* Select all chains (Protein has 3 subunits, the \"Hetero\" is the bound small molecule (inhibitor) and **continue**\n",
    "* Parametrize the ligand: upload the corresponding MOL2 file\n",
    "* Now all sorts of protein modifications are possible, but we'll omit them and **continue**\n",
    "  * Some of the steps below take a minute of two to run. You can start the downloads that you find a few cells down (starting with \"!wget\" below.\n",
    "* We use PPM2.0 to orient the protein to the membrane, and use the main subunit (Sec61α, \"PROA\") for this alignment and **continue**\n",
    "* Let's hydrate the system with 80 waters per lipid, and place it in a membrane containing a total of 400 POPC molecules. Try to balance the lipid numbers in the leaflets so that the leaflet areas agree and we don't have much tension. Here, we can create as complex of a membrane as we like, but we are not going to do a literature review on the lipidomics of the ER membrane now :) When you reach balanced leaflets, **continue**\n",
    "* Let's include 150 mM (0.15 M) of NaCl and neutralizing ions and **continue**\n",
    "* Confirm that there is no penetration (e.g. lipid tails through ring structures) and **continue**\n",
    "* Select a suitable for field combination (we will now use CHARMM36m) and simulation program (GROMACS) and **continue**\n",
    "  * If the last step takes too long, proceed with the tutorial and check the page in a few minutes.\n",
    "* Download the .tgz file containing the simulation system and the files required to run an MD simulation for it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could now decompress the tgz file, but we will instead work with the decompressed files that we have placed in the repository"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#!mv ~/Downloads/charmm-gui.tgz .\n",
    "#!tar -xvf charmm-gui.tgz\n",
    "#!ls charmm*/gromacs/\n",
    "#!mv charmm*/gromacs/* .\n",
    "#!rm -rf charmm* \n",
    "#!ls "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "CHARMM-GUI outputs handy equilibration scripts that we can now run to relax the protein and lipid structures, and get the proper density of the water. This information is included in the README file. Let's take a look at its contents. For a demonstration, we can run it for a while and then cancel it with CTRL+C"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cp -r CGUI/* .\n",
    "!cat README\n",
    "!chmod u+x README\n",
    "!./README"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we could run the entire README script, and it would eventually generate 100 ns of trajectory. But this would take 8 nodes for a day, so we will instead download 1000 ns-long trajectories from a public repository. The pre-computed trajectories are in [Zenodo](https://doi.org/10.5281/zenodo.7303653). Read the description carefully so you know what is in the files. For the next steps, we'll need a run input file (.tpr), a trajectory (.xtc), and a structure file (.gro)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!wget -O inhibitor.xtc https://zenodo.org/record/7303653/files/Sec61_KZR8445_R1.xtc?download=1\n",
    "!wget -O inhibitor.tpr https://zenodo.org/record/7303653/files/Sec61_KZR8445_R1.tpr?download=1\n",
    "!wget -O inhibitor.gro https://zenodo.org/record/7303653/files/Sec61_KZR8445_R1.gro?download=1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will also download the same file set for a system without the inhibitor present."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!wget -O noinhibitor.xtc https://zenodo.org/record/7303653/files/Sec61_noinhibitor_R1.xtc?download=1\n",
    "!wget -O noinhibitor.tpr https://zenodo.org/record/7303653/files/Sec61_noinhibitor_R1.tpr?download=1\n",
    "!wget -O noinhibitor.gro https://zenodo.org/record/7303653/files/Sec61_noinhibitor_R1.gro?download=1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now start with some basic calculations of protein structure. First, we calculate the root-mean-squared deviation (RMSD) of the protein backbone structure as a function of time with the reference taken from the TPR file (initial configuration). We will do it for the protein with the inhibitor bound, but the same calculation can be repeated for the system without the bound inhibitor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!echo \"Backbone\" \"Backbone\" | gmx rms -f inhibitor.xtc -s inhibitor.tpr -dt 1000 -o rmsd_inhi\n",
    "!echo \"Backbone\" \"Backbone\" | gmx rms -f noinhibitor.xtc -s noinhibitor.tpr -dt 1000 -o rmsd_noinhi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot the RMSD curves for the two cases."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rmsd_inhi = np.loadtxt('rmsd_inhi.xvg', comments=['#', '@'])\n",
    "rmsd_noinhi = np.loadtxt('rmsd_noinhi.xvg', comments=['#', '@'])\n",
    "plt.plot(rmsd_inhi[:, 0]/1e6, rmsd_inhi[:, 1],'red')\n",
    "plt.plot(rmsd_noinhi[:, 0]/1e6, rmsd_noinhi[:, 1],'blue')\n",
    "plt.legend(['With inhibitor', 'Without inhibitor'])\n",
    "plt.xlabel('Time (microseconds)')\n",
    "plt.ylabel('RMSD (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Is this RMSD for a protein normal? Does the inhibitor play a significant role in stabilizing the protein?\n",
    "Next, we'll find the more and less flexible regions of the protein with root mean squared fluctuation analysis of the residues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!echo \"Backbone\" | gmx rmsf -f inhibitor.xtc -s inhibitor.tpr -o rmsf_inhi -res\n",
    "!echo \"Backbone\" | gmx rmsf -f noinhibitor.xtc -s noinhibitor.tpr -o rmsf_noinhi -res"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rmsd_inhi = np.loadtxt('rmsf_inhi.xvg', comments=['#', '@'])\n",
    "rmsd_noinhi = np.loadtxt('rmsf_noinhi.xvg', comments=['#', '@'])\n",
    "plt.plot(rmsd_inhi[:, 0], rmsd_inhi[:, 1],'r*')\n",
    "plt.plot(rmsd_noinhi[:, 0], rmsd_noinhi[:, 1],'b*')\n",
    "plt.legend(['With inhibitor', 'Without inhibitor'])\n",
    "plt.xlabel('Residue no.')\n",
    "plt.ylabel('RMSD (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It seems that the region around residue 200 is more mobile without the inhibitor. You can go back to the visualization and find out which part of the protein that is.\n",
    "\n",
    "Next, we will check how much the presence of the inhibitor affects the protein conformation. The additional density found at the Sec61 lateral gate between helices 2 and 7 was used to fit the inhibitor. At this position, the inhibitor can affect the lateral gate conformation of Sec61. We will define the openness of the bate as the distance between the centers of mass of helices 2 and 7. The final conformations of these helices are visualized below for the system with the inhibitor (green, more open gate) and for the system without the inhibitor (yellow, more closed gate).\n",
    "\n",
    "<img src=\"figs/TM237.png\" width=\"300\"/>|"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u_inhi = mda.Universe('inhibitor.gro', 'inhibitor.xtc')\n",
    "\n",
    "# Define the atom selections for TMs 2 and 7\n",
    "tm2 = u_inhi.select_atoms('resid 88:96')\n",
    "tm7 = u_inhi.select_atoms('resid 288:308')\n",
    "\n",
    "# Create an empty list to store the distances\n",
    "distances_inhi = []\n",
    "time = []\n",
    "\n",
    "# Loop over all frames in the trajectory\n",
    "for ts in u_inhi.trajectory:\n",
    "    # Calculate the centers of mass for each selection\n",
    "    com2 = tm2.center_of_mass()\n",
    "    com7 = tm7.center_of_mass()\n",
    "\n",
    "    # Calculate the distance between the centers of mass\n",
    "    distance = mda.analysis.distances.distance_array(com2,com7)[0][0]\n",
    "    \n",
    "    # Append the distance to the list\n",
    "    distances_inhi.append(distance)\n",
    "    time.append(u_inhi.trajectory.time)\n",
    "    \n",
    "# Print the average distance over the trajectory\n",
    "print(\"Average distance between TM helices 2 and 7 with the inhibitor: %.3f Å\" % (sum(distances_inhi) / len(distances_inhi)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat for the system without inhibitor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u_noinhi = mda.Universe('noinhibitor.gro', 'noinhibitor.xtc')\n",
    "tm2 = u_noinhi.select_atoms('resid 88:96')\n",
    "tm7 = u_noinhi.select_atoms('resid 288:308')\n",
    "distances_noinhi = []\n",
    "\n",
    "# Loop over all frames in the trajectory\n",
    "for ts in u_noinhi.trajectory:\n",
    "    # Calculate the centers of mass for each selection\n",
    "    com2 = tm2.center_of_mass()\n",
    "    com7 = tm7.center_of_mass()\n",
    "\n",
    "    # Calculate the distance between the centers of mass\n",
    "    distance = mda.analysis.distances.distance_array(com2,com7)[0][0]\n",
    "    \n",
    "    # Append the distance to the list\n",
    "    distances_noinhi.append(distance)\n",
    "    \n",
    "# Print the average distance over the trajectory\n",
    "print(\"Average distance between TM helices 2 and 7 without the inhibitor: %.3f Å\" % (sum(distances_noinhi) / len(distances_noinhi)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the distances in the same plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(time,distances_noinhi)\n",
    "plt.plot(time,distances_inhi)\n",
    "plt.legend(['Without inhibitor', 'With inhibitor'])\n",
    "plt.xlabel('Time (microseconds)')\n",
    "plt.ylabel('Distance (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The inhibitor will clearly push the gate open and occypy that space.\n",
    "\n",
    "We will then move on to study the interactions of the inhibitor with the protein. Experiments on various Sec61 inhibitors have identified N300 and Q127 as residues that maintain the lateral gate in a closed conformation. Since the inhibitor occupies this space, it could well interact with these two residues. We will first do a simple distance calculation between these two residues and the ligand."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the atom selections for the ligand and residue 300 of the protein\n",
    "ligand = u_inhi.select_atoms('resname KZC')\n",
    "residue300 = u_inhi.select_atoms('resnum 300')\n",
    "residue127 = u_inhi.select_atoms('resnum 127')\n",
    "\n",
    "# Create an empty list to store the minimum distances\n",
    "min_distances_127 = []\n",
    "min_distances_300 = []\n",
    "time=[]\n",
    "\n",
    "# Loop over all frames in the trajectory\n",
    "for ts in u_inhi.trajectory:\n",
    "    # Calculate the distance between residue 300 and the ligand using the MDAnalysis analysis.distances module\n",
    "    distances_127 = mda.analysis.distances.distance_array(residue127.positions, ligand.positions)\n",
    "    distances_300 = mda.analysis.distances.distance_array(residue300.positions, ligand.positions)\n",
    "    # Find the minimum distance and append it to the list of minimum distances\n",
    "    min_distance_127 = np.min(distances_127)\n",
    "    min_distance_300 = np.min(distances_300)\n",
    "    min_distances_127.append(min_distance_127)\n",
    "    min_distances_300.append(min_distance_300)\n",
    "    time.append(u_inhi.trajectory.time)\n",
    "\n",
    "# Convert the minimum distances list to a NumPy array\n",
    "min_distances_127 = np.array(min_distances_127)\n",
    "min_distances_300 = np.array(min_distances_300)\n",
    "time=np.array(time)\n",
    "\n",
    "plt.plot(time/1e6,min_distances_127/10,'r-')\n",
    "plt.plot(time/1e6,min_distances_300/10,'b-')\n",
    "plt.legend(['Q127', 'N300'])\n",
    "plt.xlabel('Time (microseconds)')\n",
    "plt.ylabel('Distance (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "N300 looks like a promising hydrogen bonding candidate, whereas Q127 seems to drift further as the lateral gate is pushed open. Let's do a hydrogen bonding analysis for N300. We'll simply generate GROMACS index groups for residues 127 and 300 and calculate the time evolution of hydrogen bonds between these residues and the inhbitor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx select -f inhibitor.gro -on hbond.ndx -s inhibitor.tpr -select \"KZC = resname KZC; Q127 = resnr 127; N300 = resnr 300; Q127; N300; KZC;\"\n",
    "!echo \"Q127\" \"KZC\" | gmx hbond -f inhibitor.xtc -s inhibitor.tpr -n hbond.ndx -num hbnum_q127.xvg\n",
    "!echo \"N300\" \"KZC\" | gmx hbond -f inhibitor.xtc -s inhibitor.tpr -n hbond.ndx -num hbnum_n300.xvg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's plot the time development of the hydrogen bond numbers between the inhibitor and these two key residues."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hb_q127 = np.loadtxt('hbnum_q127.xvg', comments=['#', '@'])\n",
    "hb_n300 = np.loadtxt('hbnum_n300.xvg', comments=['#', '@'])\n",
    "plt.plot(hb_q127[:, 0]/1e6, hb_q127[:, 1],'r*')\n",
    "plt.plot(hb_n300[:, 0]/1e6, hb_n300[:, 1],'b*')\n",
    "plt.legend(['Q127', 'N300'])\n",
    "plt.xlabel('Time (microseconds)')\n",
    "plt.ylabel('Hydrogen bonds')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's a double hydrogen bond between N300 and the inhibitor!\n",
    "\n",
    "Let's now take a look at a simulation where we mutated asparagine 300 involved in hydrogen bonding with the inhibitor into nonpolar alanine"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!wget -O mutant.xtc https://zenodo.org/record/7303653/files/Sec61_KZR8445_N300A.xtc?download=1\n",
    "!wget -O mutant.tpr https://zenodo.org/record/7303653/files/Sec61_KZR8445_N300A.tpr?download=1\n",
    "!wget -O mutant.gro https://zenodo.org/record/7303653/files/Sec61_KZR8445_N300A.gro?download=1\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the atom selections for the ligand and residue 300 of the protein\n",
    "u_mut = mda.Universe('mutant.gro', 'mutant.xtc')\n",
    "\n",
    "ligand = u_mut.select_atoms('resname KZC')\n",
    "residue300 = u_mut.select_atoms('resnum 300')\n",
    "\n",
    "# Create an empty list to store the minimum distances\n",
    "min_distances_300_mut = []\n",
    "\n",
    "# Loop over all frames in the trajectory\n",
    "for ts in u_mut.trajectory:\n",
    "    # Calculate the distance between residue 300 and the ligand using the MDAnalysis analysis.distances module\n",
    "    distances_300_mut = mda.analysis.distances.distance_array(residue300.positions, ligand.positions)\n",
    "    # Find the minimum distance and append it to the list of minimum distances\n",
    "    min_distance_300_mut = np.min(distances_300_mut)\n",
    "    min_distances_300_mut.append(min_distance_300_mut)\n",
    "\n",
    "# Convert the minimum distances list to a NumPy array\n",
    "min_distances_300_mut = np.array(min_distances_300_mut)\n",
    "\n",
    "plt.plot(time/1e6,min_distances_300/10,'r-')\n",
    "plt.plot(time/1e6,min_distances_300_mut/10,'b-')\n",
    "plt.legend(['Wild type', 'N300A mutant'])\n",
    "plt.xlabel('Time (microseconds)')\n",
    "plt.ylabel('Distance (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The N300A mutation clearly leads to the detachment of the inhibitor. This is also evidenced by functional assays in which the inhibitory effect of KZR8445 is significantly reduced upon the same mutation. Here, the inhibitor clearly inhibits protein translocation as evidenced by Gaussia luciferase reporter constructs. With the N300A mutation, the inhibition decreases, and based on the MD simulations, this is due to the inhibitor leaving its binding site.\n",
    "\n",
    "<img src=\"figs/inhibition.png\" alt=\"fishy\" class=\"bg-primary\" width=\"500px\">\n",
    "(Rehan et al., bioRxiv (2022), DOI: https://doi.org/10.1101/2022.07.03.498529)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's take a look at some lipid motions. The ER membrane has some cholesterol, which is known to flip-flop between the bilayer leaflets. We'll make an excellent guess here and look at the movement of cholesterol with a residue number of 490 and plot the z coordinates of two atoms as a function of time. These are one oxygen in its polar end and one carbon at the bottom of the ring structure, and they can be used to also characterize cholesterol orientation during a flip-flop. We'll also use the list comprehension here for a more compact code.\n",
    "\n",
    "<img src=\"figs/cholesterol.png\" alt=\"fishy\" class=\"bg-primary\" width=\"50px\">"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chol490o = u_inhi.select_atoms(\"resid 490 and name O3\")\n",
    "chol490c = u_inhi.select_atoms(\"resid 490 and name C17\")\n",
    "lipids = u_inhi.select_atoms(\"resname POPC or resname POPI or resname POPS or resname POPE or resname PSM\")\n",
    "\n",
    "data = np.array([(u_inhi.trajectory.time, chol490o.positions[0][2], chol490c.positions[0][2], lipids.center_of_geometry()[2]) for ts in u_inhi.trajectory])\n",
    "time, c490o, c490c, lipcom = data.T\n",
    "plt.plot(time/1e6, c490c/10,'r-')\n",
    "plt.plot(time/1e6, c490o/10,'b-')\n",
    "plt.plot(time/1e6, lipcom/10,'k-')\n",
    "\n",
    "plt.legend(['Cholesterol oxygen', 'Cholesterol carbon', 'Lipid CoM'])\n",
    "plt.xlabel('Time (microseconds)')\n",
    "plt.ylabel('z coordinate (nm)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now take a more careful look at where the flip-flop event happens so let's only analyze a fraction of the trajectory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "chol490o = u_inhi.select_atoms(\"resid 490 and name O3\")\n",
    "chol490c = u_inhi.select_atoms(\"resid 490 and name C17\")\n",
    "lipids = u_inhi.select_atoms(\"resname POPC or resname POPI or resname POPS or resname POPE or resname PSM\")\n",
    "\n",
    "data = np.array([(u_inhi.trajectory.time, chol490o.positions[0][2], chol490c.positions[0][2], lipids.center_of_geometry()[2]) for ts in u_inhi.trajectory[870:900]])\n",
    "time, c490o, c490c, lipcom = data.T\n",
    "plt.plot(time/1e6, c490c/10,'r-')\n",
    "plt.plot(time/1e6, c490o/10,'b-')\n",
    "plt.plot(time/1e6, lipcom/10,'k-')\n",
    "\n",
    "plt.legend(['Cholesterol oxygen', 'Cholesterol carbon', 'Lipid CoM'])\n",
    "plt.xlabel('Time (microseconds)')\n",
    "plt.ylabel('z coordinate (nm)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What can you say about the mechanism of flip-flop? Does the cholesterol first change leaflet and the rotate, rotate first and then change leaflet, or do these happen simultaneously? If you want, you can write a script to analyze the tilt angle as a function of simulation time. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Does the flip-flop take place close to the Sec61 channel? Let's plot the positions (x,y) of protein Cα carbons around the time of the flip-flop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "chol490 = u_inhi.select_atoms(\"resid 490\")\n",
    "proteinca = u_inhi.select_atoms(\"protein and name CA\")\n",
    "cholxy = np.array([chol490.center_of_mass() for ts in u_inhi.trajectory[880:881]])\n",
    "protxy=[]\n",
    "\n",
    "for ts in u_inhi.trajectory[880:881]:\n",
    "    for pp in proteinca.positions:\n",
    "        protxy.append(pp[:2])\n",
    "\n",
    "protxy = np.array(protxy)\n",
    "print(protxy.shape)\n",
    "\n",
    "plt.plot(cholxy[:,1], cholxy[:,2],'rx')\n",
    "plt.plot(protxy[:,0], protxy[:,1],'bo')\n",
    "\n",
    "plt.legend(['Cholesterol CoM', 'Protein alpha carbons'])\n",
    "plt.xlabel('x coordinate (nm)')\n",
    "plt.ylabel('y coordinate (nm)')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's take a look at the membrane structure. We will calculate density profiles along the membrane normal (z direction) using the gmx density tool of GROMACS. We will plot lipid density, protein density, inhibitor density, the head group phosphorus atoms, water, and ions. We first create the default groups with gmx make_ndx and then use gmx select on the basis of those groups."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!echo \"q\" | gmx make_ndx -f inhibitor.gro -o index.ndx "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx select -f inhibitor.gro -n index.ndx -on density.ndx -s inhibitor.tpr -select \"P = name P; Lipids = resname POPI or resname POPS or resname POPE or resname POPC or resname PSM; Water = resname TIP3; Ions = resname CLA or resname POT; Inhibitor = resname KZC; Protein; Lipids; Ions; P; Inhibitor; Water;\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we run gmx density. We center the membrane lipids at every frame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!echo \"1\" \"0\" \"1\" \"2\" \"3\" \"4\" \"5\" | gmx density -f inhibitor.xtc -s inhibitor.tpr -sl 200 -ng 6 -dens number -n density.ndx -center "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "density = np.loadtxt('density.xvg', comments=['#', '@'])\n",
    "plt.plot(density[:, 0], density[:, 1],'red')\n",
    "plt.plot(density[:, 0], density[:, 2],'blue')\n",
    "plt.plot(density[:, 0], density[:, 3],'green')\n",
    "plt.plot(density[:, 0], density[:, 4],'black')\n",
    "plt.plot(density[:, 0], density[:, 5],'orange')\n",
    "plt.plot(density[:, 0], density[:, 6],'cyan')\n",
    "plt.legend(['Protein', 'Lipids', 'Ions', 'Phosphorus', 'Inhibitor', 'Water'])\n",
    "plt.xlabel('z coordinate (nm)')\n",
    "plt.ylabel('Number density (1/nm^3)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Linear scale is not optimal for the visualization of very different densities. Let's try a logarithmic y axis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(density[:, 0], density[:, 1],'red')\n",
    "plt.plot(density[:, 0], density[:, 2],'blue')\n",
    "plt.plot(density[:, 0], density[:, 3],'green')\n",
    "plt.plot(density[:, 0], density[:, 4],'black')\n",
    "plt.plot(density[:, 0], density[:, 5],'orange')\n",
    "plt.plot(density[:, 0], density[:, 6],'cyan')\n",
    "plt.legend(['Protein', 'Lipids', 'Ions', 'Phosphorus', 'Inhibitor', 'Water'])\n",
    "plt.xlabel('z coordinate (nm)')\n",
    "plt.ylabel('Number density (1/nm^3)')\n",
    "plt.yscale(\"log\")  \n",
    "plt.xlim([-5.5, 5.5])\n",
    "plt.ylim([1e-2, 2e2])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there is still time, you can try to write a code that analyses the amount of water within the hydrophobic part of the membrane, i.e. within the protein tunnel. The suitable limits for this hydrophobic region can be obtained from the phosphorus peaks in the density profiles."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  },
  "vscode": {
   "interpreter": {
    "hash": "f9f85f796d01129d0dd105a088854619f454435301f6ffec2fea96ecbd9be4ac"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
