{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial for alchemical free energy calculations using using AWH\n",
    "## Case study: Extracting the oil-water partition coefficient for a small molecule\n",
    "\n",
    "In this tutorial, we will use the accelerated weight histogram (AWH) method to calculate the partitioning free energy of a chosen small molecule between water and oil (cyclohexane). Partitioning is calculated as the difference in solvation free energies into the two solvents following the thermodynamic cycle. Here, an alchemical reaction coordinate is sampled using AWH, which provides an easy-to-use, computationally efficient, and fast-converging method to calculate PMFs along physical and alchemical reaction coordinates. The tutorial is performed using the coarse-grained Martini force field ([Martini web site](http://www.cgmartini.nl)) for speed, yet the same procedure is directly applicable to atomistic force fields. \n",
    "\n",
    " <img src=\"partitioning.png\" class=\"bg-primary\" width=\"500px\">\n",
    "\n",
    "## The workflow in a nutshell\n",
    "\n",
    "* Choose a small molecule\n",
    "* Insert the small molecule into water\n",
    "* Energy-minimize the system\n",
    "* Run an AWH simulation where the small molecule appears and disappears many times\n",
    "* Construct the PMF profile from the weight histogram and extract ΔG \n",
    "* Repeat the entire process for cyclohexane\n",
    "* Calculate the partititioning free energy ΔΔG based on the thermodynamic cycle\n",
    "\n",
    "## Learning outcomes\n",
    "\n",
    "* Set up alchemical simulations using GROMACS\n",
    "* Understand the meaning of the key simulation parameters\n",
    "* Understand the working principle of AWH\n",
    "* Understand the outputs of AWH\n",
    "\n",
    "## Alchemical transitions & Thermodynamic cycles\n",
    "\n",
    "* Partitioning free energies could be calculated using equilibrium simulations in a two-phase solvent (water & oil), but sampling could be limited in case the solute is very hydrophobic/philic. \n",
    "* Alternatively, we could use physical reaction coordinates to 'pull' the molecule from one phase to another and extract the PMF, but this typically leads to large hysteresis effects.\n",
    "* In simulations, we can also perform alchemical transitions and exploit thermodynamic cycles. \n",
    "* In an alchemical simulation, we make molecules disappear/appear, or change into other molecules.\n",
    "* Here, we use the cycle below, in which the dim solutes refer to them being decoupled from the system, i.e. they do not interact with the solvent, but retain their internal interactions. Alternatively, this could be drawn as them being in vacuum.\n",
    "* To estimate $ \\Delta G^\\mathrm{partitioning}_\\mathrm{water-oil} $, we make a round around the circle, sum all the contributions. This must be equal to 0, since we returned to the starting point, and free energy is a state function (independent of the path).\n",
    "\n",
    "$$\n",
    "\\Delta G_\\mathrm{water}^\\mathrm{solvation}+\\Delta G_\\mathrm{water-oil}^\\mathrm{partitioning}-\\Delta G_\\mathrm{oil}^\\mathrm{solvation}+\\Delta G_\\mathrm{decoupled}^\\mathrm{partitioning}=0\n",
    "$$\n",
    "\n",
    "* Moreover, the term $\\Delta G_\\mathrm{decoupled}^\\mathrm{partitioning}$, the partitioning free energy is 0.\n",
    "* Thus, we can simplify to:\n",
    "\n",
    "$$\n",
    "\\Delta G_\\mathrm{water-oil}^\\mathrm{partitioning}=\\Delta G_\\mathrm{oil}^\\mathrm{solvation}-\\Delta G_\\mathrm{water}^\\mathrm{solvation}.\n",
    "$$\n",
    "\n",
    "* This provides us with a simple way to calculate partitioning free energies based on the solvation energies into oil and water phases, which can be calculated using an alhemical simulations.\n",
    "\n",
    " <img src=\"cycle.png\" class=\"bg-primary\" width=\"500px\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Load the required libraries (?)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ipywidgets import widgets\n",
    "from IPython.display import display\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")\n",
    "import MDAnalysis as mda\n",
    "import nglview as nv\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First select a small molecule from the [Martini 3 small molecule library](https://github.com/ricalessandri/Martini3-small-molecules/). Then download the corresponding structure (.gro) file by changing the \"TOLU\" in the command below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MOLNAME=\"TOLU\"\n",
    "!echo {MOLNAME}\n",
    "!wget https://raw.githubusercontent.com/ricalessandri/Martini3-small-molecules/main/models/gros/{MOLNAME}.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generate two solvent boxes and insert the small molecule\n",
    "\n",
    "* Let's first take a look at the gmx solvate tool and its options."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx solvate -h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Look at the options. We can select the size of the simulation box with \"-box\". Alternatively, we can tell how many solvent molecules to insert with \"-maxsol\". Here, we will generate a cubic box with 800 solvent molecules. The radius needs to be adjusted to account for the large size of Martini water beads."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!gmx solvate -cp {MOLNAME}.gro -cs W.gro -box 5 5 5 -radius 0.235 -maxsol 800 -o solvated_w.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will generate the topology for the system with 1 solute molecule and 800 waters. We have a template file available, into which we will simply change the name of the chosen solute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!sed \"s/MOLECULE/{MOLNAME}/\" topol_w_template.top > topol_w.top\n",
    "!cat topol_w.top"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a look at the generated solvent system. Can you spot the solvated molecule (toluene?). If you changed the solute, change the 'resname TOLU' accordingly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "u = mda.Universe('solvated_w.gro')\n",
    "view = nv.show_mdanalysis(u)\n",
    "view.add_licorice('resname W')\n",
    "view.add_spacefill('resname TOLU')\n",
    "view.add_unitcell()\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll energy-minimize and equilibrate the system so that we obtain a correct density before production (AWH) simulations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx grompp -f MDPs/minimization.mdp -c solvated_w.gro -p topol_w.top -o min_w.tpr -maxwarn 1\n",
    "!gmx mdrun -deffnm min_w \n",
    "\n",
    "!gmx grompp -f MDPs/equilibration.mdp -c min_w.gro -p topol_w.top -o eq_w.tpr -maxwarn 1\n",
    "!gmx mdrun -deffnm eq_w -v"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see if the box size has converged. With isotropic pressure coupling all box vectors are equal (cubic box), so it is enough to look at the x component (\"Box-X\"), which can be extracted from the output files with \"gmx energy\". We then plot it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!echo \"Box-X\" | gmx energy -f eq_w.edr -o box_w.xvg\n",
    "box = np.loadtxt('box_w.xvg', comments=['#', '@'])\n",
    "plt.plot(box[:, 0], box[:, 1])\n",
    "plt.xlabel('Time (ps)')\n",
    "plt.ylabel('Box edge (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The system should look well converged, so we can proceed to the AWH simulation.\n",
    "\n",
    "## AWH simulation\n",
    "\n",
    "If we have an equilibrated system, we can proceed to the alchemical AWH run. Let's first take a look at the mdp file template. Some key parameters include (see also the comments in the mdp file with the command below):\n",
    "* **\"free energy\"** turns on the alchemical calculation\n",
    "* **\"couple-lambda0/1\"** set what is on/off at different lambda values\n",
    "* **\"couple-moltype\"** is the name of the molecule to solvate. This will be changed in the next step\n",
    "* **\"couple-intramol\"** sets whether bonded and intra-molecular non-bonded interactions are coupled. \n",
    "* **\"init-lambda-state\"** sets the state where the simulation starts defined as the element of the vectors below\n",
    "* **\"vdw/coul_lambdas\"** sets the scaling factors for LJ and electrostatic interactions for the different lambda states (here 30). The scaling parameters of the last one (0,0) correspond to a non-interacting molecule (couple-lambda0=none), and this is the initial state (init-lambda-state=30).\n",
    "* **\"sc_alpha/sigma/power\"** set the shape of the soft-core potential used to rid issues at lambda values close to 0 or 1\n",
    "* **\"awh-potential\"** must be \"umbrella\" for free energy calculations (Monte Carlo sampling)\n",
    "* **\"awh1-target\"** sets the target distribution, here flat\n",
    "* **\"awh1-growth\"** sets the two-stage method for fast convergence\n",
    "* **\"awh1-dim1-start/end\"** sets the range of lambda indices we sample (all from 0 to 30)\n",
    "* **\"awh1-error-init\"** and **\"awh1-dim1-diffusion\"** set the initial convergence rage"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat MDPs/awh.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's change the name of the molecule to couple in the mdp file to the chosen one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!sed \"s/TOCOUPLE/{MOLNAME}/\" MDPs/awh.mdp > awh.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll generate the tpr and run the awh simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx grompp -f awh.mdp -c eq_w.gro -p topol_w.top -o awh_w -maxwarn 1\n",
    "!gmx mdrun -deffnm awh_w -v -stepout 10000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyze the AWH run\n",
    "\n",
    "Here, we run the tool \"gmx awh\" to extract the PMF and other profiles stored in the energy file. The tool outputs a file every \"awh-nstout\" steps (see mdp file), so we put them in a folder, find the last file, move it to the parent folder, and delete the others."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!mkdir OUTPUT\n",
    "!gmx awh -f awh_w.edr -s awh_w.tpr -more -o OUTPUT/awh\n",
    "# The following breaks down occasionally, change to hard-coded version\n",
    "#lastfile = !ls -tr OUTPUT/*xvg | tail -n 1\n",
    "#!echo {lastfile[0]}\n",
    "#!mv {lastfile[0]} pmf_w.xvg\n",
    "!mv OUTPUT/awh_t10000.xvg pmf_w.xvg \n",
    "!rm -rf OUTPUT\n",
    "!cat pmf_w.xvg | awk '/ 0.000/ {print \"The solvation free energy for water is \" $2} /30.000/ {print \"- \" $2 \" kJ/mol\"}'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's visualize the result. Let's first plot the resulting PMF and the coordinate bias. If the PMF does not have higher curvatures than what can be described by the bias generated using Gaussians, these should look identical (the bias is needed to obtain the target distribution = flat). Here, for uncharged molecules, the first 10 lambdas are equal, as they correspond to turning of the charges. The 20 consecutive ones correspond to turning of the Lennard-Jones interactions (van der Waals forces + steric repulsion). The solvation free energy is obtained as PMF($\\lambda=1$) - PMF($\\lambda=0$). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pmf = np.loadtxt('pmf_w.xvg', comments=['#', '@'])\n",
    "plt.plot(pmf[:, 0], pmf[:, 2])\n",
    "plt.plot(pmf[:, 0], pmf[:, 1])\n",
    "plt.legend(['Coordinate bias','PMF'])\n",
    "plt.xlabel('$\\lambda$')\n",
    "plt.ylabel('PMF (kJ/mol)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now look at some other AWH outputs. The target distribution was set to be uniform, and depending on the simulation length, we will have better or worse convergence to it. The error in the target distribution can be checked from the output file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "pmf = np.loadtxt('pmf_w.xvg', comments=['#', '@'])\n",
    "plt.plot(pmf[:, 0], pmf[:, 4],'b')\n",
    "plt.plot(pmf[:, 0], pmf[:, 5],'r')\n",
    "plt.legend(['Reference value distribution','Target ref value distribution'])\n",
    "plt.xlabel('$\\lambda$')\n",
    "plt.ylabel('PMF (kJ/mol)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep error pmf_w.xvg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we'll check the friction metric, which tells how easy it is to diffuse in the alchemical space along $\\lambda$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pmf = np.loadtxt('pmf_w.xvg', comments=['#', '@'])\n",
    "plt.plot(pmf[:, 0], pmf[:, 6],'b')\n",
    "plt.ylabel('Friction metric')\n",
    "plt.xlabel('$\\lambda$')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we will finally extract the free energy of solvating the chosen solute in water."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat pmf_w.xvg | awk '/ 0.000/ {print \"The solvation free energy for water is \" $2} /30.000/ {print \"- \" $2 \" kJ/mol\"}'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will repeat the calculation for hexadecane in one block."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!gmx solvate -cp {MOLNAME}.gro -cs CHEX.gro -box 5 5 5 -maxsol 600 -o solvated_chex.gro\n",
    "!sed \"s/MOLECULE/{MOLNAME}/\" topol_chex_template.top > topol_chex.top\n",
    "!gmx grompp -f MDPs/minimization.mdp -c solvated_chex.gro -p topol_chex.top -o min_chex.tpr\n",
    "!gmx mdrun -deffnm min_chex \n",
    "!gmx grompp -f MDPs/equilibration.mdp -c min_chex.gro -p topol_chex.top -o eq_chex.tpr -maxwarn 1\n",
    "!gmx mdrun -deffnm eq_chex -v\n",
    "!gmx grompp -f awh.mdp -c eq_chex.gro -p topol_chex.top -o awh_chex.tpr -maxwarn 1\n",
    "!gmx mdrun -deffnm awh_chex -v"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir OUTPUT\n",
    "!gmx awh -f awh_chex.edr -s awh_chex.tpr -more -o OUTPUT/awh\n",
    "#lastfile = !ls -tr OUTPUT/*xvg | tail -n 1\n",
    "#!echo {lastfile[0]}\n",
    "#!mv {lastfile[0]} pmf_chex.xvg\n",
    "!mv OUTPUT/awh_t10000.xvg pmf_chex.xvg\n",
    "!rm -rf OUTPUT\n",
    "!cat pmf_chex.xvg | awk '/ 0.000/ {print \"The solvation free energy for hexadecane is \" $2} /30.000/ {print \"- \" $2 \" kJ/mol\"}'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Final questions and Conclusions\n",
    "\n",
    "Now go back to the free energy cycle in the beginning and calculate the partitioning free energy based on the solvaiton free energy values. \n",
    "\n",
    "* Is the molecule hydrophobic or hydrophilic? Does the result make sense?\n",
    "* Can you find an experimental reference value online? Does it agree with the computed one?\n",
    "\n",
    "For more information on AWH for alchemical transitions, check the [publication](\n",
    "https://doi.org/10.1063/5.0044352) and the [YouTube presentation by Berk Hess](https://www.youtube.com/watch?v=E5nGLcbyqTQ)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  },
  "vscode": {
   "interpreter": {
    "hash": "f9f85f796d01129d0dd105a088854619f454435301f6ffec2fea96ecbd9be4ac"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
