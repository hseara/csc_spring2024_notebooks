{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial for extracting the PMF of a lipid bilayer permeation using AWH"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial, we will use the accelerated weight histogram (AWH) method to calculate the potential of mean force (PMF) of a chosen small molecule along the direction normal to the plane of the membrane, which estimates the free energy profile for bilayer permeation. The AWH method provides an easy-to-use, computationally efficient, and fast-converging method to calculate PMFs along various reaction coordinates. The tutorial is performed using the coarse-grained Martini force field ([Martini web site](http://www.cgmartini.nl)) for speed, yet the same procedure is directly applicable to atomistic force fields. \n",
    " \n",
    " <img src=\"permeation.png\" class=\"bg-primary\" width=\"500px\">\n",
    "\n",
    "\n",
    "## The workflow in a nutshell\n",
    "\n",
    "* Choose a small molecule\n",
    "* Set up a coarse-grained lipid bilayer \n",
    "* Insert the small molecule into the system\n",
    "* Minimize & equilibrate\n",
    "* Run an AWH simulation where the small molecule permeates the bilayer many times\n",
    "* Construct the PMF profile from the weight histogram\n",
    "\n",
    "## Learning outcomes\n",
    "\n",
    "* Set up biased simulations using the pull code in GROMACS\n",
    "* Understand the meaning of the key simulation parameters\n",
    "* Understand the working principle of AWH\n",
    "* Understand the outputs of AWH"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "import MDAnalysis as mda\n",
    "import nglview as nv\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from ipywidgets import widgets\n",
    "from IPython.display import display\n",
    "import warnings\n",
    "warnings.filterwarnings(\"ignore\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First select a small molecule from the [Martini 3 small molecule library](https://github.com/ricalessandri/Martini3-small-molecules/) at models/gros. Then download the corresponding structure (.gro) file. Good and tested candidates with the default parameters of this tutorial are at least:\n",
    "* 2-chlorotoluene (CLTL)\n",
    "* aniline (ANIL)\n",
    "* imidazonle (IMID)\n",
    "\n",
    "\n",
    "For these molecules, the partitioning free energy from water to the bilayer is not massive, and thus they can sample the entire system relatively quickly. For others, they might stay in water / membrane throughout the short production used here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MOLNAME=\"CLTL\"\n",
    "!echo {MOLNAME}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!wget https://raw.githubusercontent.com/ricalessandri/Martini3-small-molecules/main/models/gros/{MOLNAME}.gro"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generate the lipid bilayer\n",
    "\n",
    "This is done using the [insane.py](https://doi.org/10.1021/acs.jctc.5b00209) tool. Let's see the options and select suitable ones."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!insane -h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, think what options to use to construct a lipid bilayer (bilayer.gro) and the corresponding draft for the topology file (topol.top) that contains a chosen composition (avoid charged lipids) in both leaflets. A suitable size is 8 x 8 x 12 nm^3. Use water as a solvent. A model is provided below, but feel free to change the composition. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!insane -o bilayer.gro -p topol.top -box 6,6,12 -sol W:1 -l DOPC:1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a look at the generated bilayer. It is extremely ordered, but will be relaxed quickly. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = mda.Universe('bilayer.gro')\n",
    "view = nv.show_mdanalysis(u)\n",
    "view.add_licorice(selection=\"resname W\", color='blue')\n",
    "view.add_unitcell()\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's take a look at the preliminary topology file to see what our gro file contains."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat topol.top"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The topology refers to different moleculetypes (lipids + water), but the itp files are missing. We will include topologies for:\n",
    "* General particle types (martini_v3.0.0.itp)\n",
    "* Solvents (martini_v3.0.0_solvents_v1.itp)\n",
    "* Small molecules (martini_v3.0.0_small_molecules_v1.itp)\n",
    "* Phospholipids (martini_v3.0.0_phospholipids_v1.itp)\n",
    "\n",
    "Either include these by hand into the beginning of topol.top, or run the dirty command below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!printf '%s\\n%s\\n%s\\n%s\\n' \"#include \\\"../TOP/martini_v3.0.0.itp\\\"\" \"#include \\\"../TOP/martini_v3.0.0_solvents_v1.itp\\\"\" \"#include \\\"../TOP/martini_v3.0.0_small_molecules_v1.itp\\\"\" \"#include \\\"../TOP/martini_v3.0.0_phospholipids_v1.itp\\\"\"  | cat - topol.top | grep -v \"martini.itp\" > temp\n",
    "!mv temp topol.top"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check the updated topol.top file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat topol.top"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Add the permeant to the system (structure + topology)\n",
    "\n",
    "First, we will include one molecule of the chosen type with a GROMACS command. You can take a look at the command options with the handle -h. Here, we add one molecule (-nmol 1) of the downloaded molecule (-ci). We try this so many times that the molecule certainly gets included (-try 100). The output file is called bilayer_permeant.gro (-o)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx insert-molecules -f bilayer.gro -ci {MOLNAME}.gro -nmol 1 -o bilayer_permeant.gro -try 100 -ip positions.dat -dr 3 3 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will also include the permeant molecule in the topology file (topol.top), and also print the output to see the change."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!printf '%s\\n' \"{MOLNAME} 1\" >> topol.top\n",
    "!cat topol.top"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Energy minimization and equilibration\n",
    "\n",
    "Now we will run simple energy minimization followed by 5-ns-long equilibration with a smaller time step and efficient pressure coupling to relax the bilayer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!gmx grompp -f MDPs/minimization.mdp -c bilayer_permeant.gro -p topol.top -o em.tpr -maxwarn 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx mdrun -deffnm em"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!gmx grompp -f MDPs/equilibration.mdp -c em.gro -p topol.top -o eq.tpr -maxwarn 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!gmx mdrun -deffnm eq -v"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now visualize the equilibrated bilayer. It looks quite a bit different from the initial extremely ordered system. Can you spot the permeant?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = mda.Universe('eq.gro')\n",
    "view = nv.show_mdanalysis(u)\n",
    "view.add_licorice(selection=\"resname W\", color='blue')\n",
    "view.add_unitcell()\n",
    "view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's visualize the convergence of the bilayer by looking at the time evolution of the bilayer edge length. Due to semi-isotropic pressure coupling, the membrane remains square, so area per lipid can be obtained by diviving the square of this length by the number of lipids per leaflet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!echo \"Box-X\" | gmx energy -f eq.edr -o box.xvg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "energy = np.loadtxt('box.xvg', comments=['#', '@'])\n",
    "plt.plot(energy[:, 0], energy[:, 1])\n",
    "plt.xlabel('Time (ps)')\n",
    "plt.ylabel('Box edge (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Area per lipid values for phospholipids are typically 0.50-0.70 nm^2. Is your result in the right ballpark?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## AWH simulation\n",
    "\n",
    "Next up, we will take a look at the pull and AWH options at the end of the mdp file, starting from \"pull = yes\".\n",
    "We have 2 pull groups; the reference group of \"LIPIDS\" and the one for the permeant \"PERMEANT\", which will be changed to the correct molecule name in the next step. We then define one pull coordinate based on the distance between the centres of mass of these two groups. It acts along the direction of the z axis, and uses the external AWH bias. \n",
    "\n",
    "We use a convolved AWH potential recommended for physical reaction coordinates. The target distribution of our AWH bias is 1-dimensional and cut off at 40 kJ/mol to avoid the sampmling of unlikely conformations.\n",
    "\n",
    "The coordinate will be sampled between z values of -4.5 and 4.5, which spans the bilayer and reaches some 2 nanometers to the aqueous phase on both sides of the bilayer. As our simulation system was set to be 12 nm tall, periodicity is not an issue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat MDPs/awh_template.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's change the name of the permant in the mdp file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!sed \"s/PERMEANT/{MOLNAME}/g\" MDPs/awh_template.mdp > awh.mdp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's generate an index file that contains groups for lipids and solvent. This is needed for both the definition of the pull coordinate as well as for temperature coupling groups."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!gmx select -f eq.gro -on index.ndx -s eq.tpr -select \"LIPIDS = not resname W and not resname {MOLNAME}; SOLVENT = not LIPIDS; SOLVENT; LIPIDS; {MOLNAME};\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we will generate the tpr file and run the AWH simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gmx grompp -f awh.mdp -c eq.gro -p topol.top -n index.ndx -o awh -maxwarn 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time for the production simulation. The only input parameter for AWH is really the force constant. Too small value results in the permeant not sampling the entire reaction coordinate range in this short simulation. A too large value will lead to crashes. It depends a bit on your small molecule whether the used value is enough to converge the simulation. For the proposed molecules this should not be an issue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!gmx mdrun -deffnm awh -v -stepout 10000"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyze the AWH run\n",
    "\n",
    "We will first check how the simulation has sampled different $\\lambda$ values as a function of time. It should cover them all multiple times for convergence. Here, the simulation was very short for demonstration purposes, so this might not be the case for each chosen solute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "awh = np.loadtxt('awh.xvg', comments=['#', '@'])\n",
    "plt.plot(awh[:, 0]/1000, awh[:, 1])\n",
    "plt.ylabel('$z$ coordinate')\n",
    "plt.xlabel('Time (nanoseconds)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's also take a look at the histogram of the $z$ coordinate, which better visualizes the sampling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(awh[:,1], 50,\n",
    "         histtype='bar',\n",
    "         facecolor='b')\n",
    "plt.xlabel('$z$ coordinate')\n",
    "plt.ylabel('Occurrence')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The times at which the entire reaction coordinate is covered are also written in the log file. Let's see if your molecule covered the entire interval."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep cover awh.log"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we run the tool \"gmx awh\" to extract the PMF and other profiles stored in the energy file. The tool outputs a file every \"awh-nstout\" steps (see mdp file), so we put them in a folder, find the last file, move it to the parent folder, and delete the others."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir OUTPUT\n",
    "!gmx awh -f awh.edr -s awh.tpr -more -o OUTPUT/awh\n",
    "# The following fails sometimes, so changed to a hard-coded version\n",
    "#lastfile = !ls -tr OUTPUT/*xvg | tail -n 1\n",
    "#!echo {lastfile[0]}\n",
    "#!mv {lastfile[0]} pmf.xvg\n",
    "!mv OUTPUT/awh_t100000.xvg pmf.xvg\n",
    "!rm -rf OUTPUT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then we take a look at the AWH outputs. First the PMF and the coordinate bias, which should converge to the same profile given that the curvature of the PMF is not too high at some regions, in which case the bias potential cannot capture its shape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "awh = np.loadtxt('pmf.xvg', comments=['#', '@'])\n",
    "plt.plot(awh[:, 0], awh[:, 1],'r-')\n",
    "plt.plot(awh[:, 0], awh[:, 2],'b-')\n",
    "plt.legend(['PMF','Coordinate bias'])\n",
    "plt.ylabel('PMF (kJ/mol)')\n",
    "plt.xlabel('$z$ coordinate (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also check how well we sample the target distribution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "plt.plot(awh[:, 0], awh[:, 4],'r-')\n",
    "plt.plot(awh[:, 0], awh[:, 5],'b-')\n",
    "plt.legend(['Reference value distribution','Target ref. value distribution'])\n",
    "plt.ylabel('PMF (kJ/mol)')\n",
    "plt.xlabel('$z$ coordinate (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's look at the friction metric to see which regions have the highest friction for your permeant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(awh[:, 0], awh[:, 6],'r-')\n",
    "plt.ylabel('Friction')\n",
    "plt.xlabel('$z$ coordinate (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's check the behavior of pre-computed data\n",
    "\n",
    "We will then look at the z coordinate evolution of aniline during a 10 µs-long AWH simulation to give an idea of how many transitions are sampled during a properly long AWH run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "awh = np.loadtxt('awh_anil.xvg', comments=['#', '@'])\n",
    "plt.plot(awh[:, 0]/1000, awh[:, 1])\n",
    "plt.ylabel('$z$ coordinate')\n",
    "plt.xlabel('Time (nanoseconds)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And the corresponding histogram, which seems pretty flat."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(awh[:,1], 50,\n",
    "         histtype='bar',\n",
    "         facecolor='b')\n",
    "plt.xlabel('$z$ coordinate')\n",
    "plt.ylabel('Occurrence')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And the pre-computed PMF."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "awh = np.loadtxt('pmf_anil.xvg', comments=['#', '@'])\n",
    "plt.plot(awh[:, 0], awh[:, 1],'r-')\n",
    "plt.plot(awh[:, 0], awh[:, 2],'b-')\n",
    "plt.legend(['PMF','Coordinate bias'])\n",
    "plt.ylabel('PMF (kJ/mol)')\n",
    "plt.xlabel('$z$ coordinate (nm)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Final questions and Conclusions\n",
    "\n",
    "* Did your simulation converge?\n",
    "* Is the profile symmetric for the two bilayer leaflets?\n",
    "* Where does your solute prefer to locate itself? Where are the free energy barriers?\n",
    "* From the PMF, how much is the partitioning free energy from the aqueous phase to the bilayer?\n",
    "* Does this agree with the water–oil partitioning free energy from the alchemical simulations?\n",
    "\n",
    "For more information on AWH, check the [publication](\n",
    "https://doi.org/10.1063/1.4890371) and the [YouTube presentation by Berk Hess](https://www.youtube.com/watch?v=SN-Ompgp-mE)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  },
  "vscode": {
   "interpreter": {
    "hash": "f9f85f796d01129d0dd105a088854619f454435301f6ffec2fea96ecbd9be4ac"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
