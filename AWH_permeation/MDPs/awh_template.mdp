; STANDARD MD INPUT OPTIONS FOR MARTINI 3.x
; Updated 30 Jan 2017 by PCTS
;
; for use with GROMACS 5

; TIMESTEP IN MARTINI 
; Default timestep of 20 fs. 

integrator               = md
dt                       = 0.025
nsteps                   = 4000000 ; 100 ns
nstcomm                  = 100
comm-grps		 = 

nstxout                  = 0
nstvout                  = 0
nstfout                  = 0
nstlog                   = 40000
nstenergy                = 40000
nstxout-compressed       = 4000
compressed-x-precision   = 100
compressed-x-grps        = 

; NEIGHBOURLIST and MARTINI 
; To achieve faster simulations in combination with the Verlet-neighborlist
; scheme, Martini can be simulated with a straight cutoff. In order to 
; do so, the cutoff distance is reduced 1.1 nm. 
; Neighborlist length should be optimized depending on your hardware setup:
; updating ever 20 steps should be fine for classic systems, while updating
; every 30-40 steps might be better for GPU based systems.
; The Verlet neighborlist scheme will automatically choose a proper neighborlist
; length, based on a energy drift tolerance.
;
; Coulomb interactions can alternatively be treated using a reaction-field,
; giving slightly better properties.
; Please realize that electrostVatic interactions in the Martini model are 
; not considered to be very accurate to begin with, especially as the 
; screening in the system is set to be uniform across the system with 
; a screening constant of 15. When using PME, please make sure your 
; system properties are still reasonable.
;
; With the polarizable water model, the relative electrostatic screening 
; (epsilon_r) should have a value of 2.5, representative of a low-dielectric
; apolar solvent. The polarizable water itself will perform the explicit screening
; in aqueous environment. In this case, the use of PME is more realistic.


cutoff-scheme            = Verlet
nstlist                  = 20
ns_type                  = grid
pbc                      = xyz
verlet-buffer-tolerance  = 0.005

coulombtype              = reaction-field 
rcoulomb                 = 1.1
epsilon_r                = 15	; 2.5 (with polarizable water)
epsilon_rf               = 0
vdw_type                 = cutoff  
vdw-modifier             = Potential-shift-verlet
rvdw                     = 1.1

; MARTINI and TEMPERATURE/PRESSURE
; normal temperature and pressure coupling schemes can be used. 
; It is recommended to couple individual groups in your system separately.
; Good temperature control can be achieved with the velocity rescale (V-rescale)
; thermostat using a coupling constant of the order of 1 ps. Even better 
; temperature control can be achieved by reducing the temperature coupling 
; constant to 0.1 ps, although with such tight coupling (approaching 
; the time step) one can no longer speak of a weak-coupling scheme.
; We therefore recommend a coupling time constant of at least 0.5 ps.
; The Berendsen thermostat is less suited since it does not give
; a well described thermodynamic ensemble.
; 
; Pressure can be controlled with the Parrinello-Rahman barostat, 
; with a coupling constant in the range 4-8 ps and typical compressibility 
; in the order of 10e-4 - 10e-5 bar-1. Note that, for equilibration purposes, 
; the Berendsen barostat probably gives better results, as the Parrinello-
; Rahman is prone to oscillating behaviour. For bilayer systems the pressure 
; coupling should be done semiisotropic.

tcoupl                   = v-rescale 
tc-grps                  = LIPIDS SOLVENT
tau_t                    = 1.0   1.0
ref_t                    = 320   320
Pcoupl                   = Berendsen
Pcoupltype               = semiisotropic
tau_p                    = 4.0   ;parrinello-rahman is more stable with larger tau-p, DdJ, 20130422
compressibility          = 3e-4  0
ref_p                    = 1.0   1.0

gen_vel                  = yes
gen_temp                 = 330
gen_seed                 = 473529

; MARTINI and CONSTRAINTS 
; for ring systems and stiff bonds constraints are defined
; which are best handled using Lincs. 

constraints              = none 
constraint_algorithm     = Lincs

pull                     = yes		; physical reaction coordinate
pull_ngroups             = 2   		; two groups: the permeant + reference
pull_ncoords             = 1   		; one coordinate defined by these two groups

pull-print-ref-value     = yes 		; we also print the coordinate of the reference group
pull-nstxout             = 5000		; frequency of reaction coordinate output
pull-nstfout             = 0		; no forces printed

pull_group1_name	 = LIPIDS 	; reference group
pull_group2_name         = PERMEANT	; permeant, renamed with a script

pull-group1-pbcatom	 = 1		

pull_coord1_type		= external-potential ; we use external potential from AWH
pull_coord1_potential_provider 	= AWH		     
pull_coord1_geometry           	= direction	     ; direction allows negative distances 
pull_coord1_groups         	= 1 2		     ; distance of reference + permeant
pull_coord1_dim            	= N N Y		     ; we output the Z distance. For distance
				      		     ; pull geometry, this sets how distance
						     ; is calculated (1D, 2D, 3D)
						     
pull_coord1_vec            	= 0 0 1		     ; our pull direction is along Z axis
pull-coord1-start		= yes 		     ; ref. value at start = value at start

awh			   	= yes		     ; turn AWH on
awh-potential			= convolved	     ; shape of AWH potential, default for
				  		     ; physical reaction coordinates
awh-nstout		   	= 40000		     ; frequency of xvg files from analysis
awh-nbias		   	= 1		     ; we only bias one coordinate

awh1-ndim		   	= 1		     ; dimensionality of the reaction coordinate
awh1-target			= cutoff	     ; we limit the sampling of very high barriers
awh1-target-cutoff		= 40		     ; very high barriers = 40 kJ/mol
awh1-error-init			= 10		     ; estimate of initial error, sets the initial
				  		     ; bias rate with diffusion parameter below
awh1-growth			= exp-linear	     ; two states for faster convergence

awh1-dim1-coord-provider	= pull		     ; 
awh1-dim1-coord-index	   	= 1		     ; for the first (only) AWH potential,
				  		     ; the first (only) dimension is provided by
						     ; the first (only) pull coordinate
awh1-dim1-start		   	= -4.5		     ; range of sampled values (z coordinates)
awh1-dim1-end		   	= 4.5		     ;
awh1-dim1-force-constant   	= 1e6		     ; force constant: needs to be large to capture
				  		     ; steep changes in the PMF. Will affect grid size
						     ;
awh1-dim1-diffusion	   	= 1e-4		     ; sets the initial bias rate together with
				  		     ; awh1-error-init
pull-pbc-ref-prev-step-com 	= yes