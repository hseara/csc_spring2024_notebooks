{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5ecfc613",
   "metadata": {},
   "source": [
    "# Machine Learning for MD simulations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "503dbdec",
   "metadata": {},
   "source": [
    "# Section I : PCA as a Dimensionality Reduction technique"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c9a844ab",
   "metadata": {},
   "source": [
    "[PCA](https://en.wikipedia.org/wiki/Principal_component_analysis) is a relatively simply and yet a very powerful technique that can be used for exploratory analysis of your data. Typically it is used for preparing the data for advanced treatment such as segmentation or classfication. It's chief utility is for reducing the complexity of the data detecting highly associated (\"Collective\") behavior in the data.\n",
    "\n",
    "PCA works by [eigendecomposition](https://en.wikipedia.org/wiki/Eigendecomposition_of_a_matrix) of the [Covariance matrix](https://en.wikipedia.org/wiki/Covariance_matrix) into orthogonal components. These components are uncorrelated by the virtue of being orthogonal eigenvectors of the covariance matrix. PCA is a [linear](https://en.wikipedia.org/wiki/Linear_map) technique as the original data can be expressed as a direct weighted sum of the eigenvectors. The corresponding eigenvalues of the decompoisition are a measure of the [variance](https://en.wikipedia.org/wiki/Variance) contained in each component. Technically, PCA does not directly provide [Dimensionality Reduction](https://en.wikipedia.org/wiki/Dimensionality_reduction) as the number of eigenvectors is equal to the number of input variables. However, by restricting analysis to eigenvectors with dominant eigenvalues it is possible to model the remaining eigenvectors as noise. The reduced space obtained by taking these few eigenvectors is thus used for dimensionality reduction and these vectors are called Pricipal Components. \n",
    "\n",
    "In this tutorial you will learn:\n",
    "\n",
    "    1. How to perform PCA with simple numpy functions?\n",
    "    2. How to use PCA for segmentation of a high dimensional dataset (IRIS dataset)?\n",
    "    3. How to use PCA to detect collective modes in MD simulation data?\n",
    "    4. (OPTIONAL) How K-means clustering can be combined with PCA to segment your data?\n",
    "    \n",
    "There are questions intersperesed in the tutorial. They are *all* optional. However, attempting to answer them will help you build your concepts.\n",
    "    \n",
    "Let's start by first building a simple example to demonstrate how PCA works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91501e43",
   "metadata": {},
   "outputs": [],
   "source": [
    "# imports\n",
    "\n",
    "# generic tools\n",
    "import numpy as np\n",
    "import seaborn as sns # we will use seaborn for fancy plots such as pairplots.\n",
    "import pandas as pd # we need pandas to use some seaborn functionality\n",
    "import pylab as plt # standard plottign library\n",
    "import math # standard mathematical library\n",
    "\n",
    "# tools for MD simulation trajectory analysis\n",
    "import MDAnalysis as mda # for analysing MD data\n",
    "import nglview as nv # for visualizing MD data\n",
    "from sklearn.metrics import confusion_matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c17de0c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate some correlated data\n",
    "n_points = 1000\n",
    "n_dims = 2\n",
    "data = np.random.randn(n_points, n_dims)\n",
    "data[:, 1] = 0.5*data[:, 0] + 0.5 * np.random.randn(n_points)\n",
    "# visualize the data\n",
    "plt.scatter(data[:,0],data[:,1],color = 'g', alpha = 0.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3193096",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Center the data\n",
    "# This step removes the mean from the data and puts it at the origin\n",
    "data -= np.mean(data, axis=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cfa14094",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the covariance matrix\n",
    "cov = np.cov(data.T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d810c7cb",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the eigenvalues and eigenvectors\n",
    "eig_vals, eig_vecs = np.linalg.eig(cov)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "52f999ef",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sort the eigenvalues in descending order\n",
    "idx = np.argsort(eig_vals)[::-1]\n",
    "eig_vals = eig_vals[idx]\n",
    "eig_vecs = eig_vecs[:, idx]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2f18078e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Project the data onto the first two principal components\n",
    "pca_data = np.dot(data, eig_vecs)\n",
    "\n",
    "# Plot the projected data\n",
    "plt.scatter(data[:,0],data[:,1],color = 'g', alpha = 0.5, label = 'Original')\n",
    "plt.scatter(pca_data[:, 0], pca_data[:, 1],color = 'b', alpha = 0.5, label = 'Transformed')\n",
    "plt.xlabel('First Principal Component')\n",
    "plt.ylabel('Second Principal Component')\n",
    "plt.legend()\n",
    "plt.gca().set_aspect('equal')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e47ec0f",
   "metadata": {},
   "source": [
    "**Question:** What has happened to the data due to PCA transformation?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a4da2a05",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Project the data onto the first two principal components\n",
    "pca_data = np.dot(data, eig_vecs)\n",
    "\n",
    "# Plot the projected data\n",
    "plt.scatter(data[:,0],data[:,1],color = 'g', alpha = 0.5, label = 'Original')\n",
    "\n",
    "# Plot the first two principal components as vectors\n",
    "colors = ['k','r']\n",
    "for i, scale in enumerate(eig_vals):\n",
    "    plt.arrow(0, 0, scale * eig_vecs[0, i], scale * eig_vecs[1, i],\\\n",
    "              head_width=0.2, head_length=0.1, fc=colors[i], ec=colors[i],width = 0.03)\n",
    "\n",
    "# Set the aspect ratio of the axes to 'equal'\n",
    "plt.gca().set_aspect('equal')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "975f5c11",
   "metadata": {},
   "source": [
    "**Question:** Where are the arrows pointing? Why is the second (Red) arrow shorter?\n",
    "\n",
    "**Question:** Are the two components orthogonal? How would you check that? HINT: np.dot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "248ae7d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "#### Check if the components are orthogonal.\n",
    "\n",
    "#???"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ef1c308",
   "metadata": {},
   "source": [
    "Let's now create some more complex dataset with more than two dimensions. We will create a 4D dataset and make the first and the second component correlated. We will also make the third and the fourth component anti-correlated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36543aee",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate some correlated data\n",
    "\n",
    "n_dims = 4\n",
    "data = np.random.randn(n_points, n_dims)\n",
    "data[:, 1] = 0.8*data[:, 0] + 0.5 * np.random.randn(n_points)\n",
    "data[:, 3] = -0.8*data[:, 2] + 0.5 * np.random.randn(n_points)\n",
    "\n",
    "# Center the data\n",
    "data -= np.mean(data, axis=0)\n",
    "\n",
    "# plot the data\n",
    "axes = sns.pairplot(pd.DataFrame(data))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c1ed4cd6",
   "metadata": {},
   "source": [
    "**Question**: do you see the correlation? Does it make sense? Given that the first and the second components are correlated and the third and the fourth components are correlated, what is the relation between the first and the third component?\n",
    "\n",
    "Now we will perform PCA on this data set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b6025e69",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute the covariance matrix\n",
    "cov = np.cov(data.T)\n",
    "\n",
    "# Compute the eigenvalues and eigenvectors\n",
    "eig_vals, eig_vecs = np.linalg.eig(cov)\n",
    "\n",
    "# Sort the eigenvalues in descending order\n",
    "idx = np.argsort(eig_vals)[::-1]\n",
    "eig_vals = eig_vals[idx]\n",
    "eig_vecs = eig_vecs[:, idx]\n",
    "\n",
    "# Project the data onto the first two principal components\n",
    "pca_data = np.dot(data, eig_vecs[:, :2])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44b6a52b",
   "metadata": {},
   "source": [
    "Now let's look at the (sorted) Eigenvalue spectrum for the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8e64f3d",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(range(1,n_dims+1),eig_vals)\n",
    "plt.xlabel('# Component')\n",
    "plt.ylabel(\"Eigenvalue\")\n",
    "plt.xticks(range(1,5));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e1556cb",
   "metadata": {},
   "source": [
    "**Question:** What does the eigenvalue spectrum tell us?\n",
    "\n",
    "**Question:** Recreate the data with 6 components as shown above. Make *only* the first two components correlated to each other. How many \"dominant\" i.e. large eigenvalues do you expect to see? Check your inference by plotting the eigenvalue spectrum.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd797c99",
   "metadata": {},
   "outputs": [],
   "source": [
    "# copy the code from the relevant section in this block and modify it to answer the question above.\n",
    "\n",
    "#???"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d9bb6e5",
   "metadata": {},
   "source": [
    "Let's now compile our knowledge into a single PCA function and we have all the tools we need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b10eddf7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# write the PCA function\n",
    "def PCA(X , num_components):\n",
    "     \n",
    "    #Step-1\n",
    "    X_meaned = X - np.mean(X , axis = 0)\n",
    "     \n",
    "    #Step-2\n",
    "    cov_mat = np.cov(X_meaned , rowvar = False)\n",
    "     \n",
    "    #Step-3\n",
    "    eigen_values , eigen_vectors = np.linalg.eigh(cov_mat)\n",
    "     \n",
    "    #Step-4\n",
    "    sorted_index = np.argsort(eigen_values)[::-1]\n",
    "    sorted_eigenvalue = eigen_values[sorted_index]\n",
    "    sorted_eigenvectors = eigen_vectors[:,sorted_index]\n",
    "     \n",
    "    #Step-5\n",
    "    eigenvector_subset = sorted_eigenvectors[:,0:num_components]\n",
    "     \n",
    "    #Step-6\n",
    "    X_reduced = np.dot(eigenvector_subset.transpose() , X_meaned.transpose() ).transpose()\n",
    "     \n",
    "    return X_reduced, sorted_eigenvalue, sorted_eigenvectors"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed435325",
   "metadata": {},
   "source": [
    "We will use this function to perform all the PCA related tasks in the next part of the tutorial.\n",
    "\n",
    "We are going to look at some real life datasets. A typical example is the [IRIS](https://en.wikipedia.org/wiki/Iris_flower_data_set) dataset. This data set has been historically used for statistical learning models to connect members of the Iris family of flowers to their physical characters such as Sepal and Petal lengths and widths."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ca553eb",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Get the IRIS dataset\n",
    "url = \"https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data\"\n",
    "data = pd.read_csv(url, names=['sepal length','sepal width','petal length','petal width','target'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d80b13e4",
   "metadata": {},
   "outputs": [],
   "source": [
    "data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f39d4561",
   "metadata": {},
   "outputs": [],
   "source": [
    "# visualize the raw data\n",
    "sns.pairplot(data,hue='target')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "999ee5ea",
   "metadata": {},
   "source": [
    "**Question:** Comment on this data. Can you describe which \"features\" of the data help you discern the flower family?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e97c78f",
   "metadata": {},
   "outputs": [],
   "source": [
    "#prepare the data\n",
    "x = data.iloc[:,0:4]\n",
    " \n",
    "#prepare the target\n",
    "target = data.iloc[:,4]\n",
    " \n",
    "#Applying it to PCA function\n",
    "mat_reduced, evals, evecs = PCA(x , 4)\n",
    " \n",
    "#Creating a Pandas DataFrame of reduced Dataset\n",
    "principal_df = pd.DataFrame(mat_reduced , columns = ['PC1','PC2','PC3','PC4'])\n",
    " \n",
    "#Concat it with target variable to create a complete Dataset\n",
    "principal_df = pd.concat([principal_df , pd.DataFrame(target)] , axis = 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ebbae966",
   "metadata": {},
   "outputs": [],
   "source": [
    "# visualize the PCA reduced data\n",
    "plt.figure(figsize = (6,6))\n",
    "sns.scatterplot(data = principal_df , x = 'PC1',y = 'PC2' , hue = 'target' , s = 60 , palette= 'icefire')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c962edb",
   "metadata": {},
   "source": [
    "**Questions:** \n",
    "\n",
    "    1. What do you see in the above figure in terms of the flower families? \n",
    "    \n",
    "    2. Play with the various PCA components by plotting the different possible pairs of the components in the scatter plot. What happens, say, if you plot component 2 (PC2) vs component 3 (PC3)? \n",
    "    \n",
    "    3. Visualize the eigenvalue spectrum. What does it tell you?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ec5f2b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot the other components of the PCA for IRIS data here \n",
    "# ???"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c6525313",
   "metadata": {},
   "outputs": [],
   "source": [
    "#make the eigenvalue spectrum Here\n",
    "#???"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c8d71fc",
   "metadata": {},
   "source": [
    "# PCA with MD simulations\n",
    "\n",
    "MD simulations create a very high dimensional space where extracting useful or meaningful representations from the noisy and complex dataset can be extremely challenging. The typical dimensions of MD simulations are of the order of $O(10^3$) to $O(10^4)$, giving rise to what is popularly known as [Curse of Dimensionality](https://en.wikipedia.org/wiki/Curse_of_dimensionality). PCA is one of the many techniques used to bring the complexity of the simulation space down to a reasonale degree. However, an additional step called [Feature selection](https://en.wikipedia.org/wiki/Feature_selection) is usually performed prior to Dimensionality Reduction to get rid of variables which are either of less importance or are removed to distinguish their effect from those that retained. \n",
    "\n",
    "In this case, we will be using the previously used MD data set to understand how PCA can help us connect dynamics to function. First, we will start with using only the C-alpha atom coordinates as the Features for dimensionality reduction.\n",
    "\n",
    "Dowload the following data to your current directory. This step will take a couple of minutes!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "729d6529",
   "metadata": {},
   "outputs": [],
   "source": [
    "# protein with inhibitor\n",
    "!wget -O inhibitor.xtc https://zenodo.org/record/7303653/files/Sec61_KZR8445_R1.xtc?download=1\n",
    "!wget -O inhibitor.gro https://zenodo.org/record/7303653/files/Sec61_KZR8445_R1.gro?download=1\n",
    "\n",
    "# protein without inhibitor\n",
    "!wget -O noinhibitor.xtc https://zenodo.org/record/7303653/files/Sec61_noinhibitor_R1.xtc?download=1\n",
    "!wget -O noinhibitor.gro https://zenodo.org/record/7303653/files/Sec61_noinhibitor_R1.gro?download=1\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "818590d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run the following on the data downloaded during your earlier practical\n",
    "\n",
    "# first we write out a reference structure for the analysis \n",
    "! echo \"3\" |gmx trjconv -f noinhibitor.xtc -o ref.pdb -s noinhibitor.gro -dump 0 1>output.dat 2> error.dat\n",
    "\n",
    "# Now we will write out the CA atoms for the PCA analysis from each trajectory\n",
    "! echo \"3\" |gmx trjconv -f noinhibitor.xtc -o ca_noinh.xtc -s noinhibitor.gro 1>output.dat 2> error.dat\n",
    "! echo \"3\" |gmx trjconv -f inhibitor.xtc -o ca_inh.xtc -s inhibitor.gro 1>output.dat 2> error.dat\n",
    "\n",
    "# finally, we fit the data on the reference structure we made in the first step.\n",
    "! echo \"0 0\" |gmx trjconv -f ca_noinh.xtc -o fit_noinh.xtc -s ref.pdb -fit rot+trans 1>output.dat 2> error.dat\n",
    "! echo \"0 0\" |gmx trjconv -f ca_inh.xtc -o fit_inh.xtc -s ref.pdb -fit rot+trans 1>output.dat 2> error.dat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27f1c16f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# MD simulation data\n",
    "pdb = 'ref.pdb'\n",
    "trjs = {'No inhibitor':'fit_noinh.xtc','Inhibitor':'fit_inh.xtc'} "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bf507862",
   "metadata": {},
   "outputs": [],
   "source": [
    "# watch a movie\n",
    "traj = \"fit_inh.xtc\"\n",
    "pdb = \"ref.pdb\"\n",
    "model = mda.Universe(pdb,traj )\n",
    "mda_view = nv.show_mdanalysis(model)\n",
    "mda_view"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "254bddd7",
   "metadata": {},
   "source": [
    "**Question:** What is happening in this movie? Can your describe how the protein motion affects the inhibitor binding region? This is the region roughly in the cavity inside the protein.\n",
    "\n",
    "Now, let's try analysing the data with PCA."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b46026e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make data for analysis\n",
    "coords = []\n",
    "labels_pca = []\n",
    "\n",
    "for traj in trjs.keys():\n",
    "    model = mda.Universe(pdb,trjs[traj])\n",
    "    for frame in model.trajectory:\n",
    "        coords.append(model.atoms.positions.reshape(-1))\n",
    "        labels_pca.append(traj)    \n",
    "coords = np.array(coords)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5daaacf",
   "metadata": {},
   "outputs": [],
   "source": [
    "# We will get the first 10 PCs for analysis.\n",
    "coords_reduced,evals,_ = PCA(coords,10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0fc88bc7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# covert PCA data to a pandas dataframe\n",
    "df = pd.DataFrame(coords_reduced)\n",
    "df.insert(loc = len(df.columns), column = 'Sim type', value = labels_pca)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58d40d73",
   "metadata": {},
   "outputs": [],
   "source": [
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8377451b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# make pairplot\n",
    "sns.pairplot(data = df, vars = df.columns[:-1],hue='Sim type')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7672511d",
   "metadata": {},
   "source": [
    "**Question:** So what do you see in this pairplot? Is there a PC that clearly separates the Inhibitor bound simulations from the one not bound to it?\n",
    "\n",
    "Now, let's have a look at the so-called \"eigenvalue spectrum\" of the components of the PCA."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81638846",
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.lineplot(evals)\n",
    "plt.xlim(0,20)\n",
    "plt.xticks(range(1,20));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e7d15caa",
   "metadata": {},
   "source": [
    "You notice that the eigenvalues dramatically drop past the first few components. \n",
    "\n",
    "**Question:** Does that give you a hint how many components you should choose for the reduction of dimensionality?\n",
    "\n",
    "Let's now look at the first component of the PCA. We examine it by projecting the trajectory data along the first component and then *Intrapolating* along this component between the maximum of the projection and the minimum of the projection. \n",
    "\n",
    "We will use GMX tools to do so. Please run the following cell to visualize the first component."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5455e5b3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# we will first concatinate the two trajectories into a single trajectory \n",
    "\n",
    "! gmx trjcat -cat -f fit_noinh.xtc fit_inh.xtc -o concat.xtc 1>output.dat 2> error.dat\n",
    "\n",
    "# we will now perform a covariance analysis of the concatinated trajectory to extract the PCA components.\n",
    "! echo \"0 0 \" | gmx covar -f concat.xtc -s ref.pdb 1>output.dat 2> error.dat\n",
    "\n",
    "# We now use the eigenvector analysis (anaeig) tool to visualize the first component\n",
    "! echo \"0 0\" | gmx anaeig -f concat.xtc -v eigenvec.trr -s ref.pdb -first 1 -last 1 -extr extr1.xtc -nframes 20 1>output.dat 2> error.dat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "32f85a49",
   "metadata": {},
   "outputs": [],
   "source": [
    "# watch a movie corresponding to the first PC\n",
    "traj = \"extr1.xtc\"\n",
    "pdb = \"ref.pdb\"\n",
    "model = mda.Universe(pdb,traj )\n",
    "mda_view = nv.show_mdanalysis(model)\n",
    "mda_view"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "201c020d",
   "metadata": {},
   "source": [
    "**Question:** What does the first component show? Which parts of the protein are the motions in the first component localized? How does this movie compare to the raw data we saw earlier?\n",
    "\n",
    "**Question:** Try visualizing other components. Are they different from the first component? If so, in what way? \n",
    "\n",
    "**Question:** Are the PCA components more or less noisy than the raw trajectories seen earlier? In your words try to explain why it is so."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46477394",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Using gmx anaeig write out the second component as a file extr2.xtc\n",
    "# visualize it using nglview\n",
    "\n",
    "#???"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9dde35ed",
   "metadata": {},
   "source": [
    "# Questions\n",
    "\n",
    "1. Show that the first two PCA components from the concatinated trajectories are orthogonal to each other.\n",
    "\n",
    "2. How can you create a model using the anaeig tool that contains the combined effect of the first N components? Visualize a combined model made from first two PCA components using the code above.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a2ead77f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Demonstrate the orthogonality of the PCA components from the MD data here\\\n",
    "\n",
    "#???"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a46ac4b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# using GMX tool anaeig create a a file \"extr12.xtc\" that contains the effects of the PC1 and PC2. \n",
    "# You will have to read the \"help\" section of the anaeig tool to find out how to accomplish this.\n",
    "\n",
    "#???"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6feb341c",
   "metadata": {},
   "source": [
    "# Section II : K-means as a clustering technique\n",
    "\n",
    "K-means is a simple and elegant way to quickly find clusters in your data. The algorithm works by iteratively assigning each data point to the nearest cluster centroid and then updating the centroid as the mean of all data points assigned to it. The process is repeated until convergence, meaning that the cluster assignments no longer change. One can describe the steps involved like so:\n",
    "\n",
    "    1. Choose the number of clusters (K) and obtain the data points.\n",
    "    2. Place the centroids c_1, c_2, … c_k randomly.\n",
    "    3. Repeat until convergence: a. Assign each data point to its nearest centroid. b. Update the centroid of each cluster as the mean of all data points assigned to it.\n",
    "    \n",
    "Unfortunately, K-means requires that you *know* the number of cluster beforehand, and this is one of its pitfalls. Let's code a simple case for K-means to show how it works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b06ad7c1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A simple K-means function implementation \n",
    "\n",
    "def kmeans(data, k, max_iter=100):\n",
    "    \"\"\"\n",
    "    In this function, the data are points of arbitrary dimensionality, for which we assume there are k clusters.\n",
    "    As per the algorithm in the cell above, we will repeat the search for the true cluster centers aka centroid\n",
    "    with 100 repetitions as a default.\n",
    "    \"\"\"\n",
    "    # initialize centroids randomly\n",
    "    centroids = data[np.random.choice(data.shape[0], k, replace=False)]\n",
    "\n",
    "    for i in range(max_iter):\n",
    "        # assign points to nearest centroid\n",
    "        distances = np.linalg.norm(data[:, np.newaxis] - centroids, axis=2)\n",
    "        labels = np.argmin(distances, axis=1)\n",
    "\n",
    "        # update centroids\n",
    "        new_centroids = np.array([data[labels == j].mean(axis=0) for j in range(k)])\n",
    "        if np.allclose(centroids, new_centroids):\n",
    "            break\n",
    "        centroids = new_centroids\n",
    "\n",
    "    return centroids, labels"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d84aa36",
   "metadata": {},
   "source": [
    "Now let's create a simple 2D-data set to try our implementation of K-means. We will force K-means to look for three clusters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "586e8143",
   "metadata": {},
   "outputs": [],
   "source": [
    "# define our data\n",
    "N = 2  # number of dimensions\n",
    "data = np.random.rand(100, N)  # generate random data\n",
    "k = 3  # number of clusters\n",
    "\n",
    "# perform k-means clustering\n",
    "centroids, labels = kmeans(data, k)\n",
    "\n",
    "# plot the data points and centroids\n",
    "cmap = plt.get_cmap(\"viridis\")\n",
    "colors = cmap(np.linspace(0, 1, k))\n",
    "for i in range(k):\n",
    "    plt.scatter(data[labels == i, 0], data[labels == i, 1], color=colors[i])\n",
    "plt.scatter(centroids[:, 0], centroids[:, 1], color='k', marker=\"x\",s = 100)\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"y\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8ddf9b1",
   "metadata": {},
   "source": [
    "**Question:** Is the cluster assignment reasonable?\n",
    "\n",
    "**Question:** So, what would happen if you try to find 5 clusters instead in this dataset?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "78461110",
   "metadata": {},
   "outputs": [],
   "source": [
    "# attempt to repeat the above experiment with k = 5\n",
    "# ???"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed193c75",
   "metadata": {},
   "source": [
    "This fake data above does not really contain clusters. Let's make some real Clusters to use k-means."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ffb0eca0",
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 2  # number of dimensions\n",
    "M = 3  # number of Gaussians\n",
    "n_samples = 1000  # number of samples per Gaussian\n",
    "\n",
    "# define the means for each Gaussian\n",
    "means = np.array([[-3.5, -3.5], [0, 0], [3, -3], ])\n",
    "scale = [2,1,1]\n",
    "\n",
    "# define the covariance matrices for each Gaussian. The scale variable varies the variance of the gaussians.\n",
    "covariances = np.array([np.eye(N)*scale[0], np.eye(N) * scale[1], np.eye(N) *scale[2]])\n",
    "\n",
    "# plot the samples using a colormap\n",
    "cmap = plt.get_cmap(\"viridis\")\n",
    "colors = cmap(np.linspace(0, 1, M))\n",
    "\n",
    "# generate samples from each Gaussian\n",
    "samples = np.zeros((M * n_samples, N))\n",
    "for i in range(M):\n",
    "    samples[i * n_samples:(i + 1) * n_samples] = np.random.multivariate_normal(means[i], covariances[i], n_samples)\n",
    "    \n",
    "for i in range(M):\n",
    "    plt.scatter(samples[i * n_samples:(i + 1) * n_samples, 0], samples[i * n_samples:(i + 1) * n_samples, 1], color=colors[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1eea0fef",
   "metadata": {},
   "source": [
    "**Question:** Play with the scale variable above. How does it affect the scatter plot?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cc100bc2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the data with different scale variables. Copy the code to this block and change the numbers\n",
    "# ???"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81d7afd7",
   "metadata": {},
   "source": [
    "Let's see how our K-means function fares with this data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "260837ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 3  # number of clusters\n",
    "delta = [0,0.5]\n",
    "# perform k-means clustering\n",
    "centroids, labels = kmeans(samples, k)\n",
    "\n",
    "# plot the data points and centroids\n",
    "colors = [\"r\", \"g\", \"b\"]\n",
    "for i in range(k):\n",
    "    plt.scatter(samples[labels == i, 0], samples[labels == i, 1], c=colors[i])\n",
    "    plt.text(centroids[i, 0], centroids[i, 1], str(i), color=\"white\", ha=\"center\", va=\"center\",size = 60)\n",
    "plt.scatter(centroids[:, 0], centroids[:, 1], c=\"k\", marker=\"x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e34bd11",
   "metadata": {},
   "source": [
    "Nice! With this dataset K-means assigns quite reasonable labels to the clusters! \n",
    "\n",
    "How can we check if the clustering is any good?  We use an idea called the [Contingency Table](https://en.wikipedia.org/wiki/Contingency_table). This table lists how many cluster labels were correctly assigned to a given known label and how many were assigned to an incorrect label. This means such a table can be only constructed when the ground reality is known! In our code we will represent the table as a normalized measure (row sums = 1)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95878031",
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a contingency table\n",
    "contingency_table = np.zeros((M, M), dtype=int)\n",
    "for i in range(M):\n",
    "    for j in range(M):\n",
    "        contingency_table[i, j] = np.sum(labels[i * n_samples:(i + 1) * n_samples] == j)\n",
    "\n",
    "\n",
    "contingency_table = contingency_table/contingency_table.sum(axis=1,keepdims=True)\n",
    "\n",
    "# determine the correspondence between cluster labels and Gaussian distributions\n",
    "correspondence = np.argmax(contingency_table, axis=1)\n",
    "\n",
    "# print the contingency table\n",
    "plt.imshow(contingency_table[correspondence],cmap='Blues')\n",
    "plt.colorbar()\n",
    "plt.ylabel(\"Cluster ID\")\n",
    "plt.xlabel(\"Gaussian ID\")\n",
    "plt.xticks(range(0,3));\n",
    "plt.yticks(range(0,3));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c228646",
   "metadata": {},
   "source": [
    "**Question:** How do you interpret this contingency table? \n",
    "\n",
    "**Question:** Try to recreate the contingency table by changing the scale variable. How does this variable affect the contingency table and why?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce0f13d1",
   "metadata": {},
   "source": [
    "Now, let's try the technique with some MD data we used in the PCA example. First, we select the first two dimensions of the transformed PCA data from the MD simulations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "466b3146",
   "metadata": {},
   "outputs": [],
   "source": [
    "#Create data\n",
    "data = coords_reduced[:,:2]\n",
    "\n",
    "plt.scatter(data[:,0],data[:,1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1bd8bb79",
   "metadata": {},
   "source": [
    "How many clusters do you see? Assuming there are only two clusters, let's ask K-means to find the labels for these from our data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26916ade",
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 2  # number of clusters\n",
    "# perform k-means clustering\n",
    "centroids, labels = kmeans(data, k)\n",
    "\n",
    "# plot the data points and centroids\n",
    "colors = [\"r\", \"g\", \"b\"]\n",
    "for i in range(k):\n",
    "    plt.scatter(data[labels == i, 0], data[labels == i, 1], c=colors[i],alpha=0.5)\n",
    "    plt.text(centroids[i, 0], centroids[i, 1], str(i), color=\"white\", ha=\"center\", va=\"center\",size = 40, c = 'k')\n",
    "plt.scatter(centroids[:, 0], centroids[:, 1], c=\"k\", marker=\"x\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f642871b",
   "metadata": {},
   "source": [
    "Does the clustering convince you? Let's color the data with labels from the simulations instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a5e84cb",
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.DataFrame(data)\n",
    "df.insert(loc = len(df.columns), column = 'Sim type', value = labels_pca)\n",
    "sns.pairplot(df,hue='Sim type')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e86f1ec",
   "metadata": {},
   "source": [
    "**Question:** Do the cluster labels found by K-means match the ones coming from the simulations? Remember the K-means was not aware of the simualtion labels!\n",
    "\n",
    "Let's now calculate the contingency table for this data using the simulation labels as the ground truth. In this case, we will use the function *Confusion Matrix from scikit-learn* to calculate the contingency table."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cf012b2d",
   "metadata": {},
   "outputs": [],
   "source": [
    "mask = np.where(np.array(labels_pca) == 'No inhibitor', 0, 1)\n",
    "contingency_table = confusion_matrix(labels, mask)\n",
    "contingency_table = contingency_table/contingency_table.sum(axis=1,keepdims=True)\n",
    "\n",
    "# determine the correspondence between cluster labels and Gaussian distributions\n",
    "correspondence = np.argmax(contingency_table, axis=1)\n",
    "\n",
    "# print the contingency table\n",
    "plt.imshow(contingency_table[correspondence],cmap='Blues',vmin=0, vmax=1)\n",
    "plt.colorbar()\n",
    "plt.ylabel(\"Cluster ID\")\n",
    "plt.xlabel(\"Sim type\")\n",
    "plt.xticks(range(0,2),labels=np.unique(labels_pca)[::-1]);\n",
    "plt.yticks(range(0,2));"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c1df8a6",
   "metadata": {},
   "source": [
    "As you can see the clustering method is quite easily able to disinguish between the simulation with th bound inhibitor and the one without."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe3663e1",
   "metadata": {},
   "source": [
    "**Question:** What happens to the contingency table if you use some other PCA components instead of the first one? Try with PC2 and PC3 for example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb6518f5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Try repeating the K-means clustering with the PC2 and PC3 here. Plot the contingency table. What does it show?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c1bd63e",
   "metadata": {},
   "source": [
    "# CONGRATULATIONS! You have successfully completed the exercises!! "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.11"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
