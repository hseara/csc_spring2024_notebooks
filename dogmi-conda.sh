#!/bin/bash -l

# Download and install Miniconda (yes, yes, yes...)
wget https://repo.anaconda.com/miniconda/Miniconda3-py310_23.3.1-0-Linux-x86_64.sh
bash Miniconda3-py310_23.3.1-0-Linux-x86_64.sh -b

export PATH="/home/cscuser/miniconda3/bin:$PATH"

# Initialize
conda init bash
source ~/.bashrc
conda create --name csc_spring -y
conda activate csc_spring

# Install what's needed from conda-forge
conda install -c conda-forge gromacs scikit-learn nglview "ipywidgets==7.7.2" seaborn scipy pandas jupyterlab jupyterlab_widgets nodejs mdanalysis -y

# Insane with pip
git clone https://github.com/Tsjerk/simopt.git
cd simopt
pip install .
cd ..
git clone https://github.com/Tsjerk/Insane.git
cd Insane
sed -i '20i from collections import abc' insane/lipids.py
sed -i '21i collections.MutableMapping = abc.MutableMapping' insane/lipids.py
pip install .

jupyter lab build
