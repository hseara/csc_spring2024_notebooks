conda create --name csc_spring
conda activate csc_spring
conda install -c conda-forge scikit-learn nglview "ipywidgets==7.7.2" seaborn keras tensorflow scipy pandas jupyterlab jupyterlab_widgets nodejs mdanalysis
jupyter lab build
